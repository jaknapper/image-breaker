# A checker for new OpenFlexure SD cards

A ipython notebook to run on a new SD image to make sure that the updates still allow the microscope to capture, move, scan, autofocus and record metadata.